/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_kick.c
 * \brief Includes required functions for processing the KICK command.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "channel.h"
#include "channel_mode.h"
#include "client.h"
#include "irc_string.h"
#include "ircd.h"
#include "numeric.h"
#include "send.h"
#include "modules.h"
#include "parse.h"
#include "hash.h"
#include "packet.h"
#include "s_serv.h"
#include "conf.h"


/* m_kick()
 *  parv[0] = sender prefix
 *  parv[1] = channel
 *  parv[2] = client to kick
 *  parv[3] = kick comment
 */
static void
m_kick(struct Client *client_p, struct Client *source_p,
       int parc, char *parv[])
{
  struct Client *target_p = NULL;
  struct Channel *chptr = NULL;
  char reason[KICKLEN + 1];
  const char *from, *to;
  struct Membership *ms_source = NULL;
  struct Membership *ms_target = NULL;

  if (!MyConnect(source_p) && IsCapable(source_p->from, CAP_TS6) && HasID(source_p))
  {
    from = me.id;
    to = source_p->id;
  }
  else
  {
    from = me.name;
    to = source_p->name;
  }

  if (EmptyString(parv[2]))
  {
    if (MyClient(source_p))
      sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS),
                 from, to, "KICK");
    return;
  }

  if (MyClient(source_p) && !IsFloodDone(source_p))
    flood_endgrace(source_p);

  if ((chptr = hash_find_channel(parv[1])) == NULL)
  {
    if (MyClient(source_p))
      sendto_one(source_p, form_str(ERR_NOSUCHCHANNEL),
                 from, to, parv[1]);
    return;
  }

  if (!IsServer(source_p))
  {
    if ((ms_source = find_channel_link(source_p, chptr)) == NULL)
    {
      if (MyConnect(source_p))
      {
        sendto_one(source_p, form_str(ERR_NOTONCHANNEL),
                   me.name, source_p->name, chptr->chname);
        return;
      }
    }

    if (!has_member_flags(ms_source, CHFL_OWNER | CHFL_PROTECTED | CHFL_CHANOP | CHFL_HALFOP))
    {
      /* was a user, not a server, and user isn't seen as a chanop here */
      if (MyConnect(source_p))
      {
        /* user on _my_ server, with no chanops.. so go away */
        sendto_one(source_p, form_str(ERR_CHANOPRIVSNEEDED),
                   me.name, source_p->name, chptr->chname);
        return;
      }

      if (chptr->channelts == 0)
      {
        /* If its a TS 0 channel, do it the old way */
        sendto_one(source_p, form_str(ERR_CHANOPRIVSNEEDED),
                   from, to, chptr->chname);
        return;
      }
    }
  }

  if ((target_p = find_chasing(source_p, parv[2])) == NULL)
    return;

  if ((ms_target = find_channel_link(target_p, chptr)) == NULL)
  {
    if (MyClient(source_p))
      sendto_one(source_p, form_str(ERR_USERNOTINCHANNEL),
                 from, to, target_p->name, chptr->chname);
    return;
  }

  if (MyClient(source_p) && source_p != target_p)
  {
    if (HasFlag(target_p, FLAGS_SERVICE))
    {
      char buf[IRCD_BUFSIZE];
      sprintf(buf, "%s is a %s and cannot be kicked from %s",
          target_p->name, "network service", chptr->chname);
      sendto_one(source_p, form_str(ERR_CANNOTDOCOMMAND), me.name,
          source_p->name, "KICK", buf);
      return;
    }

    if (HasUMode(target_p, UMODE_NETADMIN))
    {
      char buf[IRCD_BUFSIZE];
      sendto_one(target_p, ":%s NOTICE %s :*** %s tried to kick you from %s and failed",
          me.name, target_p->name, source_p->name, chptr->chname);
      sprintf(buf, "%s is a %s and cannot be kicked from %s",
          target_p->name, "network administrator", chptr->chname);
      sendto_one(source_p, form_str(ERR_CANNOTDOCOMMAND), me.name,
          source_p->name, "KICK", buf);
      return;
    }

#ifdef CHANAQ
    /*
     * Do not allow local clients to kick a owner or protected user unless
     * they are a owner, or the source and target are the same.
     * (client kicking himself)
     */
    if(has_member_flags(ms_target, CHFL_OWNER | CHFL_PROTECTED) && !has_member_flags(ms_source, CHFL_OWNER))
    {
      sendto_one(source_p, form_str(ERR_CHANOWNPRIVNEEDED),
          me.name, source_p->name, chptr->chname);
      return;
    }
#endif

#ifdef HALFOPS
    /* half ops cannot kick other halfops on private channels */
    if (has_member_flags(ms_source, CHFL_HALFOP) && !has_member_flags(ms_source, CHFL_OWNER | CHFL_PROTECTED | CHFL_CHANOP))
    {
      if (((chptr->mode.mode & MODE_PRIVATE) && has_member_flags(ms_target,
        CHFL_OWNER | CHFL_PROTECTED | CHFL_CHANOP | CHFL_HALFOP)) || has_member_flags(ms_target, CHFL_OWNER | CHFL_PROTECTED | CHFL_CHANOP))
      {
        sendto_one(source_p, form_str(ERR_CHANOPRIVSNEEDED),
                   me.name, source_p->name, chptr->chname);
        return;
      }
    }
#endif
  }

  if (EmptyString(parv[3]))
    strlcpy(reason, source_p->name, sizeof(reason));
  else
    strlcpy(reason, parv[3], sizeof(reason));

  /* jdc
   * - In the case of a server kicking a user (i.e. CLEARCHAN),
   *   the kick should show up as coming from the server which did
   *   the kick.
   * - Personally, flame and I believe that server kicks shouldn't
   *   be sent anyways.  Just waiting for some oper to abuse it...
   */
  if (IsServer(source_p))
    sendto_channel_local(ALL_MEMBERS, 0, chptr, ":%s KICK %s %s :%s",
                         IsHidden(source_p) || ConfigServerHide.hide_servers ? me.name : source_p->name,
                         chptr->chname, target_p->name, reason);
  else
    sendto_channel_local(ALL_MEMBERS, 0, chptr, ":%s!%s@%s KICK %s %s :%s",
                         source_p->name, source_p->username,
                         source_p->host, chptr->chname, target_p->name, reason);

  sendto_server(client_p, CAP_TS6, NOCAPS,
                ":%s KICK %s %s :%s",
                ID(source_p), chptr->chname, ID(target_p), reason);
  sendto_server(client_p, NOCAPS, CAP_TS6,
                ":%s KICK %s %s :%s", source_p->name, chptr->chname,
                target_p->name, reason);

  if (IsServer(client_p) && MyClient(target_p))
    sendto_one(client_p, ":%s KICK %s %s :Ghost member (%s (%s))", ID_or_name(&me, client_p), chptr->chname, ID_or_name(target_p, client_p), source_p->name, reason);

  remove_user_from_channel(ms_target);
}

static struct Message kick_msgtab =
{
  "KICK", 0, 0, 3, MAXPARA, MFLG_SLOW, 0,
  { m_unregistered, m_kick, m_kick, m_ignore, m_kick, m_ignore }
};

static void
module_init(void)
{
  mod_add_cmd(&kick_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&kick_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
  .flags   = MODULE_FLAG_CORE
};
