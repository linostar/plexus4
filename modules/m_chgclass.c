/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 2014 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "modules.h"
#include "conf.h"
#include "memory.h"

/* CHGCLASS target class */
static void
me_chgclass(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p;

  if (!HasFlag(source_p, FLAGS_SERVICE))
    return;

  target_p = find_person(client_p, parv[1]);

  /* accept my client or an unknown */
  if (target_p == NULL || (!MyClient(target_p) && !IsUnknown(target_p)))
    return;

  struct ClassItem *class = class_find(parv[2], 1);
  if (class == NULL)
    return;

  struct MaskItem *conf = conf_make(CONF_CLIENT);
  conf->user  = xstrdup("chgclass");
  conf->host  = xstrdup("chgclass");
  conf->class = class;

  if (attach_conf(target_p, conf))
    conf_free(conf);
}

static struct Message chgclass_msgtab = {
  "CHGCLASS", 0, 0, 3, MAXPARA, MFLG_SLOW, 0,
  { m_ignore, m_ignore, m_ignore, me_chgclass, m_ignore, m_ignore }
};

static void
module_init(void)
{
  mod_add_cmd(&chgclass_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&chgclass_msgtab);
}

struct module module_entry = {
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};

