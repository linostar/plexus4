/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_invite.c
 * \brief Includes required functions for processing the INVITE command.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "channel.h"
#include "channel_mode.h"
#include "client.h"
#include "hash.h"
#include "irc_string.h"
#include "ircd.h"
#include "numeric.h"
#include "send.h"
#include "conf.h"
#include "s_serv.h"
#include "parse.h"
#include "modules.h"
#include "packet.h"
#include "s_user.h"


/*
** m_invite
**      parv[0] - sender prefix
**      parv[1] - user to invite
**      parv[2] - channel name
**      parv[3] - invite timestamp
*/
static void
m_invite(struct Client *client_p, struct Client *source_p,
         int parc, char *parv[])
{
  struct Client *target_p = NULL;
  struct Channel *chptr = NULL;
  struct Membership *ms = NULL;

  if (!IsClient(source_p))
    return;

  if (EmptyString(parv[2]))
  {
    sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS),
               me.name, source_p->name, "INVITE");
    return;
  }

  if (MyClient(source_p) && !IsFloodDone(source_p))
    flood_endgrace(source_p);

  if ((target_p = find_person(client_p, parv[1])) == NULL)
  {
    sendto_one(source_p, form_str(ERR_NOSUCHNICK),
               me.name, source_p->name, parv[1]);
    return;
  }

  if ((chptr = hash_find_channel(parv[2])) == NULL)
  {
    sendto_one(source_p, form_str(ERR_NOSUCHCHANNEL),
               me.name, source_p->name, parv[2]);
    return;
  }

  if (MyConnect(source_p))
  {
    if ((ms = find_channel_link(source_p, chptr)) == NULL)
    {
      sendto_one(source_p, form_str(ERR_NOTONCHANNEL),
                 me.name, source_p->name, chptr->chname);
      return;
    }

    if (chptr->mode.mode & (MODE_INVITEONLY | MODE_PRIVATE))
      if (!has_member_flags(ms, CHFL_OWNER | CHFL_PROTECTED | CHFL_CHANOP | CHFL_HALFOP))
      {
        sendto_one(source_p, form_str(ERR_CHANOPRIVSNEEDED),
                   me.name, source_p->name, chptr->chname);
        return;
      }

    if (IsMember(target_p, chptr))
    {
      sendto_one(source_p, form_str(ERR_USERONCHANNEL), me.name,
                 source_p->name, target_p->name, chptr->chname);
      return;
    }

    if (target_check_client(source_p, target_p))
    {
      if (!HasFlag(source_p, FLAGS_TARGET_NOTICED))
      {
        sendto_snomask(SNO_BOTS, L_ALL | SNO_ROUTE, "Possible target change flooder (invite) %s",
                       get_client_name(source_p, HIDE_IP));
        AddFlag(source_p, FLAGS_TARGET_NOTICED);
      }

      sendto_one(source_p, form_str(ERR_TARGCHANGE), me.name, source_p->name, target_p->name, source_p->localClient->next_target - CurrentTime);
      return;
    }

    DelFlag(source_p, FLAGS_TARGET_NOTICED);
  }

  if (IsMember(target_p, chptr))
    return;

  if (MyClient(target_p))
  {
    if (HasUMode(target_p, UMODE_CALLERID | UMODE_SOFTCALLERID))
    {
      if (!accept_message(source_p, target_p) && !target_recently_messaged(source_p, target_p) && !HasFlag(source_p, FLAGS_SERVICE) && !HasUMode(source_p, UMODE_OPER))
      {
        int notsoft = !!HasUMode(target_p, UMODE_CALLERID);

        sendto_one(source_p, form_str(RPL_TARGUMODEG),
                   ID_or_name(&me, source_p),
                   ID_or_name(source_p, source_p), target_p->name,
                   notsoft ? "+g" : "+G",
                   notsoft ? "server side ignore" :
                             "server side ignore with the exception of common channels");

        if ((target_p->localClient->last_caller_id_time + ConfigFileEntry.caller_id_wait) < CurrentTime)
        {
          sendto_one(source_p, form_str(RPL_TARGNOTIFY),
                     ID_or_name(&me, source_p),
                     ID_or_name(source_p, source_p), target_p->name);

          sendto_one(target_p, form_str(RPL_UMODEGMSG),
                     me.name, target_p->name,
                     source_p->name, source_p->username, HasUMode(target_p, UMODE_OPER) ? source_p->realhost : source_p->host,
                     notsoft ? "+g" : "+G");

          target_p->localClient->last_caller_id_time = CurrentTime;
        }

        return;
      }
    }
  }

  if (MyConnect(source_p))
  {
    if (ConfigChannel.invite_throttle && !HasUMode(source_p, UMODE_OPER))
    {
      struct Invite *invite = find_invite_to(chptr, target_p, source_p);

      if (invite && invite->created + ConfigChannel.invite_throttle > CurrentTime)
      {
        sendto_one(source_p, form_str(RPL_INVITETHROTTLE),
                   me.name, source_p->name,
                   target_p->name, chptr->chname,
                   target_p->name, chptr->chname);
        return;
      }

      if (dlink_list_length(&source_p->localClient->invites) >= ConfigChannel.max_chans_per_user)
      {
        sendto_one(source_p, form_str(RPL_INVITETHROTTLE),
                   me.name, source_p->name,
                   target_p->name, chptr->chname,
                   target_p->name, chptr->chname);
        return;
      }
    }

    sendto_one(source_p, form_str(RPL_INVITING), me.name,
               source_p->name, target_p->name, chptr->chname);

    if (target_p->away[0])
      sendto_one(source_p, form_str(RPL_AWAY),
                 me.name, source_p->name, target_p->name,
                 target_p->away);

    if ((chptr->mode.mode & MODE_INVITEONLY) && (chptr->mode.mode & MODE_PRIVATE))
    {
      sendto_channel_butone(NULL, &me, chptr, CHFL_OWNER | CHFL_PROTECTED | CHFL_CHANOP | CHFL_HALFOP, 0,
                            "NOTICE %%%s :%s is inviting %s to %s.",
                            chptr->chname, source_p->name,
                            target_p->name, chptr->chname);
    }
  }
  else if (parc > 3 && IsDigit(*parv[3]))
    if (atoi(parv[3]) > chptr->channelts)
      return;

  if (MyConnect(target_p))
  {
    sendto_one(target_p, ":%s!%s@%s INVITE %s :%s",
               source_p->name, source_p->username,
               source_p->host,
               target_p->name, chptr->chname);
  }

  add_invite(chptr, target_p, source_p);

  sendto_server(client_p, CAP_TS6, NOCAPS, ":%s INVITE %s %s %lu",
             ID(source_p),
             ID(target_p),
             chptr->chname, (unsigned long)chptr->channelts);
  sendto_server(client_p, NOCAPS, CAP_TS6, ":%s INVITE %s %s %lu",
             source_p->name,
             target_p->name,
             chptr->chname, (unsigned long)chptr->channelts);
}

static struct Message invite_msgtab =
{
  "INVITE", 0, 0, 3, MAXPARA, MFLG_SLOW, 0,
  { m_unregistered, m_invite, m_invite, m_ignore, m_invite, m_ignore }
};

static void
module_init(void)
{
  mod_add_cmd(&invite_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&invite_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
