/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2005-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_services.c
 * \brief Includes required functions for processing the CS/NS/MS/OS/BS services command.
 * \version $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "channel_mode.h"
#include "numeric.h"
#include "conf.h"
#include "s_serv.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "irc_string.h"
#include "s_user.h"
#include "hash.h"

static void
m_identify(struct Client *client_p, struct Client *source_p,
          int parc, char *parv[])
{
  struct Client *target_p = hash_find_server_name(ConfigFileEntry.service_name);

  if (!target_p)
  {
    sendto_one(source_p, form_str(ERR_SERVICESDOWN), me.name, source_p->name, "NickServ");
    return;
  }

  if (parc < 2)
  {
      sendto_one(source_p, ":%s NOTICE %s :Syntax: IDENTIFY [account] <password> ",
                           me.name, source_p->name);
      return;
  }

  sendto_one(target_p, ":%s PRIVMSG NickServ@%s :IDENTIFY %s", ID_or_name(source_p, target_p), ConfigFileEntry.service_name, parv[1]);
}

static void
me_su(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p;

  if(!IsServer(source_p))
    return;

  target_p = find_person(client_p, parv[1]);
  if (target_p == NULL)
    return;

  if(EmptyString(parv[2]))
    target_p->suser[0] = '\0';
  else
    strlcpy(target_p->suser, parv[2], sizeof(target_p->suser));
}

static void
me_svsnoop(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  if (!HasFlag(source_p, FLAGS_SERVICE))
    return;

  if (parv[1][0] == '+')
  {
    dlink_node *ptr, *next_ptr;

    svsnoop = 1;
    sendto_snomask(SNO_ALL, L_ALL, "This server has been placed in NOOP mode");

    DLINK_FOREACH_SAFE(ptr, next_ptr, oper_list.head)
    {
      struct Client *target_p = ptr->data;

      /* find flags already set for user */
      unsigned int setflags = target_p->umodes;
      struct Snomasks setsnomask = target_p->localClient->snomask;

      deoper(target_p);

      send_umode_out(target_p, target_p, setflags, &setsnomask);
    }
  }
  else
  {
    svsnoop = 0;
    sendto_snomask(SNO_ALL, L_ALL, "This server is no longer in NOOP mode");
  }
}

static struct Message identify_msgtab = {
  "IDENTIFY", 0, 0, 0, 1, MFLG_SLOW, 0,
  { m_unregistered, m_identify, m_ignore, m_ignore, m_identify, m_ignore }
};

static struct Message su_msgtab = {
  "SU", 0, 0, 2, MAXPARA, MFLG_SLOW, 0,
  { m_ignore, m_ignore, m_ignore, me_su, m_ignore, m_ignore }
};

static struct Message svsnoop_msgtab = {
  "SVSNOOP", 0, 0, 2, MAXPARA, MFLG_SLOW, 0,
  { m_ignore, m_ignore, m_ignore, me_svsnoop, m_ignore, m_ignore }
};

static void
module_init(void)
{
  mod_add_cmd(&identify_msgtab);
  mod_add_cmd(&su_msgtab);
  mod_add_cmd(&svsnoop_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&identify_msgtab);
  mod_del_cmd(&su_msgtab);
  mod_del_cmd(&svsnoop_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
