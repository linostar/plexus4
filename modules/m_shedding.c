/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *  m_shedding.c: Enables/disables user shedding.
 *
 *  Copyright (C) 2002 by the past and present ircd coders, and others.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "channel.h"
#include "client.h"
#include "conf.h"
#include "hostmask.h"
#include "numeric.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "event.h"
#include "irc_string.h"
#include "listener.h"
#include "hook.h"

#define SHED_RATE_MIN 5

static int shedding;
static int rate;
static struct MaskItem *redir;

static struct MaskItem *
find_redir()
{
  for (int i = 0; i < ATABLE_SIZE; ++i)
  {
    dlink_node *ptr;

    DLINK_FOREACH(ptr, atable[i].head)
    {
      struct AddressRec *arec = ptr->data;
      struct MaskItem *conf = arec->conf;

      if (!IsConfClient(conf) || !IsConfRedir(conf))
        continue;

      if (strcmp(conf->user, "*") || strcmp(conf->host, "*"))
        continue;

      return conf;
    }
  }

  return NULL;
}

static void
send_redir(struct Client *client_p)
{
  if (redir && redir->name)
  {
    int port;

    if (redir->port)
      port = redir->port;
    else
      port = client_p->localClient->listener ? client_p->localClient->listener->port : PORTNUM;

    sendto_one(client_p, form_str(RPL_REDIR),
               me.name, client_p->name,
               redir->name,
               port);
  }
}

static void
user_shedding_shed(void *unused)
{
  dlink_node *ptr;
  
  DLINK_FOREACH_PREV(ptr, local_client_list.tail)
  {
      struct Client *client_p = ptr->data;

      if (HasUMode(client_p, UMODE_OPER))
        continue;

      send_redir(client_p);

      exit_client(client_p, &me, "Server closed connection");
      break;
  }

  eventDelete(user_shedding_shed, NULL);
}

static void
user_shedding_main(void *unused)
{
  int deviation = (rate / (3+(int) (7.0f*rand()/(RAND_MAX+1.0f))));

  eventAddIsh("user shedding shed event", user_shedding_shed, NULL, rate+deviation);
}

static void
shed_off()
{
  eventDelete(user_shedding_main, NULL);
  eventDelete(user_shedding_shed, NULL);

  if (redir)
  {
    --redir->ref_count;
    redir = NULL;
  }

  shedding = 0;
  rate = 0;
}

static void
shed_on(int r)
{
  shed_off();

  redir = find_redir();
  if (redir)
  {
    ++redir->ref_count;
  }

  shedding = 1;
  rate = r;

  eventAdd("user shedding main event", user_shedding_main, NULL, rate);
}

static void
mo_shedding(struct Client *client_p, struct Client *source_p, int parc, char **parv)
{
  if (!HasUMode(source_p, UMODE_ROUTING))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVILEGES), me.name, source_p->name);
    return;
  }

  if (irccmp(parv[1], "OFF") == 0)
  {
    shed_off();
    sendto_snomask(SNO_ALL, L_ALL,
                   "User shedding DISABLED by %s", source_p->name);
    return;
  }

  int rate = atoi(parv[1]);
  if (rate < SHED_RATE_MIN)
  {
    sendto_one(source_p, ":%s NOTICE %s :Rate may not be less than %d", me.name, source_p->name, SHED_RATE_MIN);
    return;
  }

  shed_off();
    
  sendto_snomask(SNO_ALL, L_ALL,
                 "User shedding ENABLED by %s. Shedding interval: %d seconds", 
                 source_p->name, rate);

  rate -= (rate/5);

  shed_on(rate);
}

static void
shedding_local_user_register(struct local_user_register_data *data)
{
  struct Client *client_p;

  if (!shedding)
    return;

  client_p = data->client;

  if (IsExemptLimits(client_p))
    return;

  send_redir(client_p);

  exit_client(client_p, &me, "Use an alternative server");
}

static struct Message shedding_msgtab =
{
  "SHEDDING", 0, 0, 2, MAXPARA, MFLG_SLOW, 0,
   { m_unregistered, m_not_oper, m_ignore, m_ignore, mo_shedding, m_ignore }
};

static struct Event local_user_register_event =
{
  .event = &local_user_register_hook,
  .handler = shedding_local_user_register
};

static void
module_init(void)
{
  mod_add_cmd(&shedding_msgtab);
  hook_add(&local_user_register_event);
}

static void
module_exit(void)
{
  mod_del_cmd(&shedding_msgtab);
  hook_del(&local_user_register_event);

  shed_off();
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};

