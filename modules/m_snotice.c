/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014 plexxus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "irc_string.h"
#include "numeric.h"
#include "send.h"
#include "s_serv.h"
#include "parse.h"
#include "modules.h"

static void
ms_snotice(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *mask = parv[1],
             *message = parv[2];
  struct Snomask *snomask;

  if (!IsServer(source_p))
    return;

  if (EmptyString(message))
    return;

  snomask = snomask_find(*mask);

  sendto_snomask_from(source_p, snomask, L_ALL | SNO_REMOTE | SNO_ROUTE, message);
}

static struct Message snotice_msgtab =
{
  "SNOTICE", 0, 0, 3, MAXPARA, MFLG_SLOW, 0,
  { m_unregistered, m_ignore, ms_snotice, m_ignore, m_ignore, m_ignore }
};

static void
module_init(void)
{
  mod_add_cmd(&snotice_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&snotice_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
