/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1999 Bahamut development team.
 *  Copyright (c) 2011-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_svsmode.c
 * \brief Includes required functions for processing the SVSMODE command.
 * \version $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "s_serv.h"
#include "send.h"
#include "channel_mode.h"
#include "parse.h"
#include "modules.h"
#include "irc_string.h"
#include "s_user.h"
#include "conf.h"


/*! \brief SVSMODE command handler (called by services)
 *
 * \param client_p Pointer to allocated Client struct with physical connection
 *                 to this server, i.e. with an open socket connected.
 * \param source_p Pointer to allocated Client struct from which the message
 *                 originally comes from.  This can be a local or remote client.
 * \param parc     Integer holding the number of supplied arguments.
 * \param parv     Argument vector where parv[0] .. parv[parc-1] are non-NULL
 *                 pointers.
 * \note Valid arguments for this command are:
 *      - parv[0] = sender prefix
 *      - parv[1] = nickname
 *      - parv[2] = TS (or mode, depending on svs version)
 *      - parv[3] = mode (or services id if old svs version)
 *      - parv[4] = optional argument (services id)
 */
static void
me_svsmode(struct Client *client_p, struct Client *source_p,
           int parc, char *parv[])
{
  struct Client *target_p = NULL;
  int what = MODE_ADD;
  unsigned int flag = 0, setmodes = 0;
  const char *m = NULL, *modes = NULL, *extarg = NULL;
  time_t ts = 0;

  if (!HasFlag(source_p, FLAGS_SERVICE))
    return;

  if ((parc >= 4) && ((*parv[3] == '+') || (*parv[3] == '-')))
  {
    ts     = atol(parv[2]);
    modes  = parv[3];
    extarg = (parc > 4) ? parv[4] : NULL;
  }
  else
  {
    modes  = parv[2];
    extarg = (parc > 3) ? parv[3] : NULL;
  }

  if ((target_p = find_person(client_p, parv[1])) == NULL)
    return;

  if (ts && (ts != target_p->tsinfo))
    return;

  setmodes = target_p->umodes;

  for (m = modes; *m; ++m)
  {
    switch (*m)
    {
      case '+':
        what = MODE_ADD;
        break;
      case '-':
        what = MODE_DEL;
        break;

      case 'd':
        if (!EmptyString(extarg))
          strlcpy(target_p->svid, extarg, sizeof(target_p->svid));
        break;

      case 'x':
        if (what == MODE_ADD)
          AddUMode(target_p, UMODE_CLOAK);
        else
        {
          if (!HasUMode(target_p, UMODE_CLOAK))
            break;

          DelUMode(target_p, flag);

          if (!MyClient(target_p))
            break;

          user_set_hostmask(target_p, NULL, target_p->realhost);

          sendto_server(NULL, CAP_ENCAP | CAP_TS6, NOCAPS,
              ":%s ENCAP * CHGHOST %s %s",
              ID(&me), ID(target_p), target_p->host);
          sendto_server(NULL, CAP_ENCAP, CAP_TS6,
              ":%s ENCAP * CHGHOST %s %s",
              me.name, target_p->name, target_p->host);
        }
        break;

      case 'o':
        if (what == MODE_DEL)
          deoper(target_p);

        break;

      case 'i':
        if (what == MODE_ADD && !HasUMode(target_p, UMODE_INVISIBLE))
        {
          AddUMode(target_p, UMODE_INVISIBLE);
          ++Count.invisi;
        }

        if (what == MODE_DEL && HasUMode(target_p, UMODE_INVISIBLE))
        {
          DelUMode(target_p, UMODE_INVISIBLE);
          --Count.invisi;
        }

        break;

      case 'S':  /* Only servers may set +S in a burst */
        break;

      case 's':
        if (MyClient(target_p))
        {
          if (what == MODE_ADD)
          {
            if (!EmptyString(extarg))
            {
              struct Snomasks add, remove;
              snomask_parse(extarg, &add, &remove);

              snomask_set(&target_p->localClient->snomask, &add, &remove);
            }
            else
            {
              struct Snomasks masks;
              masks.local = ConfigFileEntry.oper_snomasks.local;
              masks.remote = ConfigFileEntry.oper_snomasks.remote;

              snomask_set(&target_p->localClient->snomask, &masks, NULL);
            }
          }
          else
          {
            struct Snomasks masks;
            masks.local = SNOMASK_MASK;
            masks.remote = SNOMASK_MASK;

            snomask_set(&target_p->localClient->snomask, NULL, &masks);
          }
        }
        /* FALLTHROUGH */

      default:
        if ((flag = user_modes[(unsigned char)*m]))
        {
          if (what == MODE_ADD)
            AddUMode(target_p, flag);
          else
            DelUMode(target_p, flag);
        }

        break;
    }
  }

  if (MyConnect(target_p) && (setmodes != target_p->umodes))
  {
    char modebuf[IRCD_BUFSIZE];

    send_umode(target_p, target_p, setmodes, NULL, modebuf);
  }
}

static struct Message svsmode_msgtab =
{
  "SVSMODE", 0, 0, 3, MAXPARA, MFLG_SLOW, 0,
  { m_ignore, m_ignore, m_ignore, me_svsmode, m_ignore, m_ignore }
};

static void
module_init(void)
{
  mod_add_cmd(&svsmode_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&svsmode_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
