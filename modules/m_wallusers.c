/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "irc_string.h"
#include "numeric.h"
#include "send.h"
#include "s_user.h"
#include "parse.h"
#include "modules.h"
#include "s_serv.h"

static void
mo_wallusers(struct Client *client_p, struct Client *source_p,
           int parc, char *parv[])
{
  const char *message = parv[1];

  if (!HasOFlag(source_p, OPER_FLAG_WALLUSERS))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVS), me.name,
               source_p->name, "wallusers");
    return;
  }

  if (EmptyString(message))
  {
    sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS),
               me.name, source_p->name, "WALLUSERS");
    return;
  }

  sendto_wallops_flags(UMODE_WALLOP, source_p, "WALLUSERS - %s", message);
  sendto_wallusers_flags(UMODE_WALLOP, UMODE_OPER, source_p, "%s", message);

  sendto_server(NULL, CAP_TS6, NOCAPS,
                ":%s WALLUSERS :%s", ID(source_p), message);
  sendto_server(NULL, NOCAPS, CAP_TS6,
                ":%s WALLUSERS :%s", source_p->name, message);
}

static void
ms_wallusers(struct Client *client_p, struct Client *source_p,
           int parc, char *parv[])
{
  const char *message = parv[1];

  if (EmptyString(message))
    return;

  sendto_wallops_flags(UMODE_WALLOP, source_p, "WALLUSERS - %s", message);
  sendto_wallusers_flags(UMODE_WALLOP, UMODE_OPER, source_p, "%s", message);

  sendto_server(client_p, CAP_TS6, NOCAPS,
                ":%s WALLUSERS :%s", ID(source_p), message);
  sendto_server(client_p, NOCAPS, CAP_TS6,
                ":%s WALLUSERS :%s", source_p->name, message);
}

static struct Message wallusers_msgtab =
{
  "WALLUSERS", 0, 0, 2, MAXPARA, MFLG_SLOW, 0,
  { m_unregistered, m_not_oper, ms_wallusers, m_ignore, mo_wallusers, m_ignore }
};

static void
module_init(void)
{
  mod_add_cmd(&wallusers_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&wallusers_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
