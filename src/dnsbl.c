/************************************************************************
 *   IRC - Internet Relay Chat
 *   Copyright (C) 2013 Plexus Development Team
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include "client.h"
#include "dnsbl.h"
#include "conf.h"
#include "hostmask.h"
#include "s_misc.h"
#include "ircd.h"
#include "send.h"
#include "list.h"
#include "irc_res.h"
#include "s_user.h"
#include "irc_string.h"
#include "memory.h"
#include "log.h"
#include "numeric.h"

static void
dnsbl_callback(void *vptr, const struct sockaddr_in *addr, const char *name)
{
  struct DnsblLookup *lookup = vptr;
  struct Client *cptr = lookup->cptr;
  struct dnsbl_entry *entry = lookup->entry;
  struct ip_entry *ip = NULL;
  int hit = 0;

  if (addr != NULL && (addr->sin_addr.s_addr & 0x00FFFFFF) == 0x0000007F)
  {
    uint8_t result = addr->sin_addr.s_addr >> 24;
    if (entry->codes[result].i)
    {
      ++entry->hits;

      /* Add the result to ipcache */
      ip = find_or_add_ip(&cptr->localClient->ip);
      ip->dnsbl_entry = entry;
      ip->dnsbl_result = result;

      hit = 1;
    }
  }

  /* remove and free lookup */
  dlinkDelete(&lookup->node, &cptr->localClient->dnsbl_queries);
  dlinkDelete(&lookup->enode, &entry->lookups);

  MyFree(lookup);

  if (hit)
  {
    /* now that they're listed in something we can drop further queries as we don't need them */
    clear_dnsbl_lookup(cptr);
  }
  else if (!dlink_list_length(&cptr->localClient->dnsbl_queries))
  {
    /* this was the last query, mark ip as clear in ipcache */
    if (ip == NULL)
      ip = find_or_add_ip(&cptr->localClient->ip);
    ip->dnsbl_clear = 1;
  }

  if (IsUnknown(cptr))
  {
    /* maybe they can register now? */
    if (!cptr->localClient->registration)
      register_local_user(cptr);
  }
  else if (hit)
  {
    /* if hit, ip must be valid */
    assert(ip != NULL);

    /* ban if this is my client */
    dnsbl_ban(cptr, ip);
  }
}


/*
 * start_dnsbl_lookup
 *
 * inputs       - struct Client *
 * side effects - The client is checked for being on the DNS blacklists.
 */
void
start_dnsbl_lookup(struct Client *cptr)
{
  dlink_node *ptr;
  struct ip_entry *ipc;
  char ipbuf[RFC1035_MAX_DOMAIN_LENGTH + 1];

  assert(MyConnect(cptr));

  if (dlink_list_length(&dnsbl_items) == 0)
    return;

  ipc = find_or_add_ip(&cptr->localClient->ip);
  if (ipc->dnsbl_clear)
  {
    /* cache miss */
    ++ServerStats.is_rblc;
    return;
  }

  if (ipc->dnsbl_entry != NULL)
  {
    /* cache hit */
    ++ServerStats.is_rblh;
    ipc->dnsbl_cached = 1;
    /* ban if this is my client */
    dnsbl_ban(cptr, ipc);
    return;
  }

  if (cptr->localClient->ip.ss.ss_family == AF_INET)
  {
    uint8_t *ip = (uint8_t *) &((struct sockaddr_in *) &cptr->localClient->ip)->sin_addr.s_addr;
    snprintf(ipbuf, sizeof(ipbuf), "%u.%u.%u.%u", (unsigned int) ip[3], (unsigned int) ip[2], (unsigned int) ip[1], (unsigned int) ip[0]);
  }
  else if (cptr->localClient->ip.ss.ss_family == AF_INET6)
  {
    char *ip = (char *) ((struct sockaddr_in6 *) &cptr->localClient->ip)->sin6_addr.s6_addr;
    char dest[33]; /* 16*2 + 1 */
    char *bufptr = ipbuf;

    base16_encode(dest, sizeof(dest), ip, 16);

    /* skip trailing \0 */
    for (int i = sizeof(dest) - 2; i >= 0; --i)
    {
      *bufptr++ = dest[i];
      *bufptr++ = '.';
    }

    *bufptr = 0;
  }
  else
    return;

  DLINK_FOREACH(ptr, dnsbl_items.head)
  {
    struct dnsbl_entry *dentry = ptr->data;
    char hostbuf[RFC1035_MAX_DOMAIN_LENGTH + 1];
    struct DnsblLookup *info;
    
    info = MyMalloc(sizeof(struct DnsblLookup));
    dlinkAdd(info, &info->node, &cptr->localClient->dnsbl_queries);
    dlinkAdd(info, &info->enode, &dentry->lookups);

    snprintf(hostbuf, sizeof(hostbuf), "%s.%s", ipbuf, dentry->name);

    info->cptr = cptr;
    info->entry = dentry;

    gethost_byname_type(&info->query, (dns_callback_fnc) dnsbl_callback, info, hostbuf, T_A);
  }
}

static void
cancel_lookup(struct DnsblLookup *info)
{
    delete_resolver_query(&info->query);

    dlinkDelete(&info->node, &info->cptr->localClient->dnsbl_queries);
    dlinkDelete(&info->enode, &info->entry->lookups);

    MyFree(info);
}

/*
 * clear_dnsbl_lookup
 *
 * inputs       - struct Client *
 * side effects - Pending DNS queries for this client will be canceled
 */
void
clear_dnsbl_lookup(struct Client *cptr)
{
  dlink_node *ptr, *next_ptr;

  assert(MyConnect(cptr));

  DLINK_FOREACH_SAFE(ptr, next_ptr, cptr->localClient->dnsbl_queries.head)
  {
    struct DnsblLookup *info = ptr->data;
    cancel_lookup(info);
  }
}

void
cancel_blacklist(struct dnsbl_entry *bl)
{
  dlink_node *ptr, *next_ptr;

  /* cancel all associated lookups */
  DLINK_FOREACH_SAFE(ptr, next_ptr, bl->lookups.head)
  {
    struct DnsblLookup *info = ptr->data;
    cancel_lookup(info);
  }

  /* remove blacklist from ipcache */
  ipcache_cleanup_dnsbl(bl);
}

const char *
dnsbl_format_reason(struct Client *source_p, struct ip_entry *ip)
{
  static char buf[IRCD_BUFSIZE];
  char codebuf[4];

  strlcpy(buf, ip->dnsbl_entry->reason, sizeof(buf));

  str_replace(buf, buf, sizeof(buf), "%i", source_p->sockhost);
  str_replace(buf, buf, sizeof(buf), "%d", smalldate(CurrentTime, 0));
  snprintf(codebuf, sizeof(codebuf), "%u", ip->dnsbl_result);
  str_replace(buf, buf, sizeof(buf), "%c", codebuf);

  return buf;
}

void
dnsbl_ban(struct Client *source_p, struct ip_entry *ip)
{
  const char *user_reason, *channel_reason;

  /* unknown clients are rejected later at registration */
  if (!MyClient(source_p) || IsExemptDnsbl(source_p))
    return;

  user_reason = dnsbl_format_reason(source_p, ip);
  channel_reason = !EmptyString(ConfigFileEntry.kline_reason) ? ConfigFileEntry.kline_reason : user_reason;

  sendto_snomask(SNO_REJ, L_ALL,
                 "Exiting %s, listed in DNSBL %s with result %d%s",
                 get_client_name(source_p, SHOW_IP), ip->dnsbl_entry->name, ip->dnsbl_result, ip->dnsbl_cached ? " (cached)" : "");

  sendto_one(source_p, form_str(ERR_YOUREBANNEDCREEP),
             me.name, source_p->name, user_reason);

  exit_client(source_p, &me, channel_reason);
}

