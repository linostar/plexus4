/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "log.h"
#include "list.h"
#include "memory.h"
#include "modules.h"
#include "client.h"
#include "send.h"
#include "hook.h"

#ifdef HAVE_SWIG_LUA

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include "swig.h"

extern int luaopen_plexus(lua_State *);

static dlink_list scripts;

struct lua_script
{
  lua_State *l;
  dlink_node node;
};

void
lua_loadfile(const char *file)
{
  const char *path = mod_find_file(file);
  if (path == NULL)
  {
    ilog(LOG_TYPE_IRCD, "Cannot locate script %s", file);
    return;
  }

  lua_State *l = luaL_newstate();

  luaL_openlibs(l);
  luaopen_plexus(l);

  int err = luaL_dofile(l, path);

  if (err)
  {
    const char *error = lua_tostring(l, -1);
    ilog(LOG_TYPE_IRCD, "%s", error);
    sendto_snomask(SNO_DEBUG, L_ADMIN, "%s", error);
    lua_pop(l, 1);
    lua_close(l);
    return;
  }

  struct lua_script *script = MyMalloc(sizeof(struct lua_script));
  script->l = l;
  dlinkAdd(script, &script->node, &scripts);

  ilog(LOG_TYPE_IRCD, "Loaded script %s", file);
}

void
lua_unload_all()
{
  dlink_node *node, *node2;

  DLINK_FOREACH_SAFE(node, node2, scripts.head)
  {
    struct lua_script *script = node->data;

    lua_close(script->l);
    dlinkDelete(&script->node, &scripts);
    MyFree(script);
  }
}

static void
lua_do_call(struct lua_script *script, struct hook *hook, void *arg)
{
  lua_getglobal(script->l, hook->name);
  if (lua_isnil(script->l, -1))
  {
    lua_pop(script->l, 1);
    return;
  }

  int args = 0;
  if (arg && hook->type)
  {
    swig_type_info *info = SWIG_TypeQuery(script->l, hook->type);
    if (info == NULL)
      lua_pushnil(script->l);
    else
      SWIG_Lua_NewPointerObj(script->l, arg, info, 0);
    ++args;
  }

  int err = lua_pcall(script->l, args, 0, 0);
  if (err)
  {
    const char *error = lua_tostring(script->l, -1);
    ilog(LOG_TYPE_IRCD, "%s", error);
    sendto_snomask(SNO_DEBUG, L_ADMIN, "%s", error);
    lua_pop(script->l, 1);
  }
}

void
lua_callhook(struct hook *hook, void *arg)
{
  dlink_node *node;

  DLINK_FOREACH(node, scripts.head)
  {
    struct lua_script *script = node->data;
    lua_do_call(script, hook, arg);
  }
}

#else

void
lua_loadfile(const char *file)
{
  ilog(LOG_TYPE_IRCD, "Scripting support is not enabled, cannot load %s", file);
}

void
lua_unload_all()
{
}

void
lua_callhook(struct hook *hook, void *arg)
{
}

#endif

