%module plexus
%{

#include "stdinc.h"
#include "list.h"
#include "fdlist.h"
#include "s_bsd.h"
#include "client.h"
#include "dbuf.h"
#include "event.h"
#include "irc_string.h"
#include "ircd.h"
#include "listener.h"
#include "numeric.h"
#include "packet.h"
#include "irc_res.h"
#include "restart.h"
#include "s_auth.h"
#include "conf.h"
#include "log.h"
#include "s_serv.h"
#include "send.h"
#include "memory.h"
#include "s_user.h"
#include "s_misc.h"
#include "hook.h"
#include "channel.h"
#include "channel_mode.h"

#define malloc MyMalloc
#define calloc MyCalloc
#define realloc MyRealloc
#define free MyFree

%}

%ignore dbuf_put_args;

%warnfilter(301,314,451,452,462);

%include "stdinc.h"
%include "list.h"
%include "fdlist.h"
%include "s_bsd.h"
%include "client.h"
%include "dbuf.h"
%include "event.h"
%include "irc_string.h"
%include "ircd.h"
%include "listener.h"
%include "numeric.h"
%include "packet.h"
%include "irc_res.h"
%include "restart.h"
%include "s_auth.h"
%include "conf.h"
%include "log.h"
%include "s_serv.h"
%include "send.h"
%include "memory.h"
%include "s_user.h"
%include "s_misc.h"
%include "hook.h"
%include "channel.h"
%include "channel_mode.h"

