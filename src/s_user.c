/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file s_user.c
 * \brief User related functions.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "s_user.h"
#include "s_misc.h"
#include "channel.h"
#include "channel_mode.h"
#include "client.h"
#include "fdlist.h"
#include "hash.h"
#include "irc_string.h"
#include "s_bsd.h"
#include "ircd.h"
#include "listener.h"
#include "motd.h"
#include "numeric.h"
#include "conf.h"
#include "log.h"
#include "s_serv.h"
#include "send.h"
#include "whowas.h"
#include "memory.h"
#include "packet.h"
#include "rng_mt.h"
#include "userhost.h"
#include "s_misc.h"
#include "parse.h"
#include "watch.h"
#include "cloak.h"
#include "dnsbl.h"
#include "hook.h"

static char umode_buffer[IRCD_BUFSIZE], cmode_buffer[IRCD_BUFSIZE];

static void user_welcome(struct Client *);
static void report_and_set_user_flags(struct Client *, const struct MaskItem *);
static int check_xline(struct Client *);
static void introduce_client(struct Client *);

/* Used for building up the isupport string,
 * used with init_isupport, add_isupport, delete_isupport
 */

struct Isupport
{
  dlink_node node;
  char *name;
  char *options;
  int number;
};

static dlink_list support_list;
static dlink_list support_list_lines;

/* memory is cheap. map 0-255 to equivalent mode */
const unsigned int user_modes[256] =
{
  /* 0x00 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0x0F */
  /* 0x10 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0x1F */
  /* 0x20 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0x2F */
  /* 0x30 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0x3F */
  0,                  /* @ */
  0,                  /* A */
  0,                  /* B */
  UMODE_NOCTCP,       /* C */
  UMODE_DEAF,         /* D */
  0,                  /* E */
  0,                  /* F */
  UMODE_SOFTCALLERID, /* G */
  0,                  /* H */
  0,                  /* I */
  0,                  /* J */
  0,                  /* K */
  0,                  /* L */
  0,                  /* M */
  UMODE_NETADMIN,     /* N */
  0,                  /* O */
  0,                  /* P */
  0,                  /* Q */
  UMODE_REGONLY,      /* R */
  UMODE_SSL,          /* S */
  0,                  /* T */
  UMODE_SERVICE,      /* U */
  0,                  /* V */
  UMODE_WEBIRC,       /* W */
  0,                  /* X */
  0,                  /* Y */
  0,                  /* Z 0x5A */
  0, 0, 0, 0, 0,      /* 0x5F   */
  0,                  /* 0x60   */
  UMODE_ADMIN,        /* a */
  0,                  /* b */
  0,                  /* c */
  0,                  /* d */
  0,                  /* e */
  0,                  /* f */
  UMODE_CALLERID,     /* g */
  0,                  /* h */
  UMODE_INVISIBLE,    /* i */
  0,                  /* j */
  0,                  /* k */
  UMODE_LOCOPS,       /* l */
  0,                  /* m */
  0,                  /* n */
  UMODE_OPER,         /* o */
  UMODE_HIDECHANNELS, /* p */
  UMODE_ROUTING,      /* q */
  UMODE_REGISTERED,   /* r */
  UMODE_SERVNOTICE,   /* s */
  0,                  /* t */
  0,                  /* u */
  0,                  /* v */
  UMODE_WALLOP,       /* w */
  UMODE_CLOAK,        /* x */
  UMODE_SPY,          /* y */
  UMODE_OPERWALL,     /* z      0x7A */
  0,0,0,0,0,          /* 0x7B - 0x7F */

  /* 0x80 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0x8F */
  /* 0x90 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0x9F */
  /* 0xA0 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0xAF */
  /* 0xB0 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0xBF */
  /* 0xC0 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0xCF */
  /* 0xD0 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0xDF */
  /* 0xE0 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0xEF */
  /* 0xF0 */ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0  /* 0xFF */
};

int
is_noctcp(struct Client *source_p, struct Client *target_p)
{
  if(!MyClient(target_p) || HasUMode(source_p, UMODE_SERVICE) || HasUMode(source_p, UMODE_OPER) || source_p == target_p)
    return 0;

  if (HasUMode(target_p, UMODE_NOCTCP))
    return 1;

  return 0;
}

void
assemble_umode_buffer(void)
{
  unsigned int idx = 0;
  char *umode_buffer_ptr = umode_buffer;

  for (; idx < (sizeof(user_modes) / sizeof(user_modes[0])); ++idx)
    if (user_modes[idx])
      *umode_buffer_ptr++ = idx;

  *umode_buffer_ptr = '\0';
}

void
assemble_cmode_buffer(void)
{
  char *p = cmode_buffer;
  int i;

  for (i = 0; mode_table[i].func; ++i)
    if (mode_table[i].letter)
      if (!ConfigFileEntry.disable_chmodes || !strchr(ConfigFileEntry.disable_chmodes, mode_table[i].letter))
        *p++ = mode_table[i].letter;

  *p++ = ' ';

  for (i = 0; mode_table[i].func; ++i)
    if (!mode_table[i].mode && mode_table[i].letter)
      if (!ConfigFileEntry.disable_chmodes || !strchr(ConfigFileEntry.disable_chmodes, mode_table[i].letter))
        *p++ = mode_table[i].letter;

  *p++ = 0;
}

/* show_lusers()
 *
 * inputs       - pointer to client
 * output       - NONE
 * side effects - display to client user counts etc.
 */
void
show_lusers(struct Client *source_p)
{
  const char *from, *to;
  int nonhidden_servers = 0;

  if (!MyConnect(source_p) && IsCapable(source_p->from, CAP_TS6) && HasID(source_p))
  {
    from = me.id;
    to = source_p->id;
  }
  else
  {
    from = me.name;
    to = source_p->name;
  }

  if (!ConfigServerHide.hide_servers || HasUMode(source_p, UMODE_OPER))
    sendto_one(source_p, form_str(RPL_LUSERCLIENT),
               from, to, (Count.total-Count.invisi),
               Count.invisi, dlink_list_length(&global_serv_list));
  else
  {
    dlink_node *ptr;

    DLINK_FOREACH(ptr, global_serv_list.head)
    {
      struct Client *server = ptr->data;
      if (!IsHidden(server))
        ++nonhidden_servers;
    }

    sendto_one(source_p, form_str(RPL_LUSERCLIENT), from, to,
               (Count.total-Count.invisi), Count.invisi, nonhidden_servers);
  }

  if (Count.oper > 0)
    sendto_one(source_p, form_str(RPL_LUSEROP),
               from, to, Count.oper);

  if (dlink_list_length(&unknown_list) > 0)
    sendto_one(source_p, form_str(RPL_LUSERUNKNOWN),
               from, to, dlink_list_length(&unknown_list));

  if (dlink_list_length(&global_channel_list) > 0)
    sendto_one(source_p, form_str(RPL_LUSERCHANNELS),
               from, to, dlink_list_length(&global_channel_list));

  if (!ConfigServerHide.hide_servers || HasUMode(source_p, UMODE_OPER))
  {
    sendto_one(source_p, form_str(RPL_LUSERME),
               from, to, Count.local, Count.myserver);
    sendto_one(source_p, form_str(RPL_LOCALUSERS),
               from, to, Count.local, Count.max_loc);
  }
  else
  {
    sendto_one(source_p, form_str(RPL_LUSERME),
               from, to, Count.total, nonhidden_servers);
    sendto_one(source_p, form_str(RPL_LOCALUSERS),
               from, to, Count.total, Count.max_tot);
  }

  sendto_one(source_p, form_str(RPL_GLOBALUSERS),
             from, to, Count.total, Count.max_tot);

  if (!ConfigServerHide.hide_servers || HasUMode(source_p, UMODE_OPER))
    sendto_one(source_p, form_str(RPL_STATSCONN), from, to,
               Count.max_loc_con, Count.max_loc_cli, Count.totalrestartcount);

  if (Count.local > Count.max_loc_cli)
    Count.max_loc_cli = Count.local;

  if ((Count.local + Count.myserver) > Count.max_loc_con)
    Count.max_loc_con = Count.local + Count.myserver;
}

/* show_isupport()
 *
 * inputs       - pointer to client
 * output       - NONE
 * side effects	- display to client what we support (for them)
 */
void
show_isupport(struct Client *source_p)
{
  const dlink_node *ptr = NULL;

  DLINK_FOREACH(ptr, support_list_lines.head)
    sendto_one(source_p, form_str(RPL_ISUPPORT), me.name,
               source_p->name, ptr->data);
}

/*
** register_local_user
**      This function is called when both NICK and USER messages
**      have been accepted for the client, in whatever order. Only
**      after this, is the USER message propagated.
**
**      NICK's must be propagated at once when received, although
**      it would be better to delay them too until full info is
**      available. Doing it is not so simple though, would have
**      to implement the following:
**
**      (actually it has been implemented already for a while) -orabidoo
**
**      1) user telnets in and gives only "NICK foobar" and waits
**      2) another user far away logs in normally with the nick
**         "foobar" (quite legal, as this server didn't propagate
**         it).
**      3) now this server gets nick "foobar" from outside, but
**         has alread the same defined locally. Current server
**         would just issue "KILL foobar" to clean out dups. But,
**         this is not fair. It should actually request another
**         nick from local user or kill him/her...
*/
void
register_local_user(struct Client *source_p)
{
  const struct MaskItem *conf = NULL;

  assert(source_p != NULL);
  assert(source_p == source_p->from);
  assert(MyConnect(source_p));
  assert(IsUnknown(source_p));
  assert(!source_p->localClient->registration);

  ClearCap(source_p, CAP_TS6);

  if (ConfigFileEntry.ping_cookie)
  {
    if (!IsPingSent(source_p) && !source_p->localClient->random_ping)
    {
      do
        source_p->localClient->random_ping = genrand_int32();
      while (!source_p->localClient->random_ping);

      sendto_one(source_p, "PING :%u",
                 source_p->localClient->random_ping);
      SetPingSent(source_p);
      return;
    }

    if (!HasPingCookie(source_p))
      return;
  }

  /* still doing dnsbl queries? */
  if (dlink_list_length(&source_p->localClient->dnsbl_queries))
    return;

  source_p->localClient->last_privmsg = CurrentTime;

  if (!check_client(source_p))
    return;

  if (!valid_hostname(source_p->host))
  {
    sendto_one(source_p, ":%s NOTICE %s :*** Notice -- You have an illegal "
               "character in your hostname", me.name, source_p->name);
    strlcpy(source_p->host, source_p->sockhost,
            sizeof(source_p->host));
    strlcpy(source_p->realhost, source_p->sockhost, sizeof(source_p->realhost));
  }

  conf = source_p->localClient->confs.head->data;

  if (!IsGotId(source_p))
  {
    char username[USERLEN + 1];
    const char *p = username;
    unsigned int i = 0;
    int is_no_tilde;
    dlink_node *ptr;

    if (IsNeedIdentd(conf))
    {
      ++ServerStats.is_ref;
      sendto_one(source_p, ":%s NOTICE %s :*** Notice -- You need to install "
                 "identd to use this server", me.name, source_p->name);
      exit_client(source_p, &me, "Install identd");
      return;
    }

    strlcpy(username, source_p->username, sizeof(username));

    is_no_tilde = 0;
    DLINK_FOREACH(ptr, source_p->localClient->confs.head)
      is_no_tilde |= IsNoTilde((struct MaskItem *) ptr->data);

    if (!is_no_tilde)
      source_p->username[i++] = '~';

    for (; *p && i < USERLEN; ++p)
      if (*p != '[')
        source_p->username[i++] = *p;

    source_p->username[i] = '\0';
  }

  /* not authed to sasl but required to? */
  if (IsConfNeedAccount(conf) && EmptyString(source_p->suser))
  {
    ++ServerStats.is_ref;
    sendto_one(source_p, ":%s NOTICE %s :*** Notice -- You need to identify "
               "via SASL to use this server", me.name, source_p->name);
    exit_client(source_p, &me, "Use SASL authentication");
    return;
  }

  /* password check */
  if (!EmptyString(conf->passwd))
  {
    const char *pass = source_p->localClient->passwd;

    if (!match_conf_password(pass, conf))
    {
      ++ServerStats.is_ref;
      sendto_one(source_p, form_str(ERR_PASSWDMISMATCH),
                 me.name, source_p->name);
      exit_client(source_p, &me, "Bad Password");
      return;
    }
  }

  /* don't free source_p->localClient->passwd here - it can be required
   * by masked /stats I if there are auth{} blocks with need_password = no;
   * --adx
   */

  /* check dnsbl result in ipcache */
  if (!IsConfExemptDnsbl(conf))
  {
    struct ip_entry *ip = find_or_add_ip(&source_p->localClient->ip);
    if (ip->dnsbl_entry != NULL)
    {
      const char *reason = dnsbl_format_reason(source_p, ip);

      sendto_snomask(SNO_REJ, L_ALL,
                     "Rejecting %s, listed in DNSBL %s with result %d%s",
                     get_client_name(source_p, SHOW_IP), ip->dnsbl_entry->name, ip->dnsbl_result, ip->dnsbl_cached ? " (cached)" : "");

      exit_client(source_p, &me, reason);
      return;
    }
  }

  /* report if user has &^>= etc. and set flags as needed in source_p */
  report_and_set_user_flags(source_p, conf);

  abort_sasl(source_p);

  struct local_user_register_data data = {
    .client = source_p
  };
  hook_execute(&local_user_register_hook, &data);

  if (IsDead(source_p))
    return;

  /* Limit clients -
   * We want to be able to have servers and F-line clients
   * connect, so save room for "buffer" connections.
   * Smaller servers may want to decrease this, and it should
   * probably be just a percentage of the MAXCLIENTS...
   *   -Taner
   */
  /* Except "F:" clients */
  if ((Count.local >= ServerInfo.max_clients + MAX_BUFFER) ||
      (Count.local >= ServerInfo.max_clients && !IsExemptLimits(source_p)))
  {
    sendto_snomask(SNO_FULL, L_ALL,
                   "Too many clients, rejecting %s[%s].",
                   source_p->name, source_p->realhost);
    ++ServerStats.is_ref;
    exit_client(source_p, &me, "Sorry, server is full - try later");
    return;
  }

  /* valid user name check */
  if (!valid_username(source_p->username, 1))
  {
    char tmpstr2[IRCD_BUFSIZE];

    sendto_snomask(SNO_REJ, L_ALL,
                   "Invalid username: %s (%s@%s)",
                   source_p->name, source_p->username, source_p->realhost);
    ++ServerStats.is_ref;
    snprintf(tmpstr2, sizeof(tmpstr2), "Invalid username [%s]",
             source_p->username);
    exit_client(source_p, &me, tmpstr2);
    return;
  }

  if (!IsExemptKline(source_p))
    if (check_xline(source_p))
      return;

  if (EmptyString(source_p->id))
  {
    const char *id = NULL;

    while (hash_find_id((id = uid_get())) != NULL)
      ;

    strlcpy(source_p->id, id, sizeof(source_p->id));
    hash_add_id(source_p);
  }

  if (!valid_uid(source_p->id))
  {
    exit_client(source_p, &me, "Bogus UID");
    return;
  }

  sendto_snomask(SNO_CCONN, L_ALL,
                 "Client connecting: %s (%s@%s) [%s] {%s} [%s]",
                 source_p->name, source_p->username, source_p->realhost,
                 ConfigFileEntry.hide_spoof_ips && IsIPSpoof(source_p) ?
                 "255.255.255.255" : source_p->sockhost,
                 get_client_class(&source_p->localClient->confs),
                 source_p->info);

  if (ConfigFileEntry.invisible_on_connect)
  {
    AddUMode(source_p, UMODE_INVISIBLE);
    ++Count.invisi;
  }

  if (ConfigFileEntry.enable_cloak_system && !IsIPSpoof(source_p))
  {
    make_virthost(source_p->realhost, source_p->cloaked_host, sizeof(source_p->cloaked_host));
    make_virthost(source_p->sockhost, source_p->cloaked_ip, sizeof(source_p->cloaked_ip));

    strlcpy(source_p->host, source_p->cloaked_host, sizeof(source_p->host));

    AddUMode(source_p, UMODE_CLOAK);
  }

  if (++Count.local > Count.max_loc)
  {
    Count.max_loc = Count.local;

    if (!(Count.max_loc % 10))
      sendto_snomask(SNO_ALL, L_ALL,
                     "New Max Local Clients: %d",
                     Count.max_loc);
  }

  /* Increment our total user count here */
  if (++Count.total > Count.max_tot)
    Count.max_tot = Count.total;
  ++Count.totalrestartcount;

  assert(source_p->servptr == &me);
  SetClient(source_p);
  dlinkAdd(source_p, &source_p->lnode, &source_p->servptr->serv->client_list);

  assert(dlinkFind(&unknown_list, source_p));

  dlink_move_node(&source_p->localClient->lclient_node,
                  &unknown_list, &local_client_list);

  user_welcome(source_p);

  assert(!IsUserHostIp(source_p));
  add_user_host(source_p->username, source_p->realhost, 0);
  SetUserHost(source_p);

  introduce_client(source_p);
}

/* register_remote_user()
 *
 * inputs       - source_p remote or directly connected client
 *              - username to register as
 *              - host name to register as
 *              - server name
 * output	- NONE
 * side effects	- This function is called when a remote client
 *		  is introduced by a server.
 */
void
register_remote_user(struct Client *source_p,
                     const char *username, const char *host, const char *server,
                     const char *realhost)
{
  struct Client *target_p = NULL;

  assert(source_p != NULL);
  assert(source_p->username != username);

  strlcpy(source_p->host, host, sizeof(source_p->host));
  strlcpy(source_p->realhost, realhost, sizeof(source_p->realhost));
  strlcpy(source_p->username, username, sizeof(source_p->username));

  /*
   * coming from another server, take the servers word for it
   */
  source_p->servptr = hash_find_server(server);

  /*
   * Super GhostDetect:
   * If we can't find the server the user is supposed to be on,
   * then simply blow the user away.        -Taner
   */
  if (source_p->servptr == NULL)
  {
    sendto_snomask(SNO_ALL, L_ALL,
                   "No server %s for user %s[%s@%s] from %s",
                   server, source_p->name, source_p->username,
                   source_p->host, source_p->from->name);
    kill_client(source_p->from, source_p, "%s (Server doesn't exist)", me.name);

    AddFlag(source_p, FLAGS_KILLED);
    exit_client(source_p, &me, "Ghosted Client");
    return;
  }

  /* this can't happen anymore with ts6 as the user's server is taken from the source */
  if ((target_p = source_p->servptr) && target_p->from != source_p->from)
  {
    sendto_snomask(SNO_DEBUG, L_ALL,
                   "Bad User [%s] :%s USER %s@%s %s, != %s[%s]",
                   source_p->from->name, source_p->name, source_p->username,
                   source_p->host, source_p->servptr->name,
                   target_p->name, target_p->from->name);
    kill_client(source_p->from, source_p,
                "%s (NICK from wrong direction (%s != %s))",
                me.name, source_p->servptr->name, target_p->from->name);
    AddFlag(source_p, FLAGS_KILLED);
    exit_client(source_p, &me, "USER server wrong direction");
    return;
  }

  /*
   * If the nick has been introduced by a services server,
   * make it a service as well.
   */
  if (HasFlag(source_p->servptr, FLAGS_SERVICE))
    AddFlag(source_p, FLAGS_SERVICE);

  /* Increment our total user count here */
  if (++Count.total > Count.max_tot)
    Count.max_tot = Count.total;

  SetClient(source_p);
  dlinkAdd(source_p, &source_p->lnode, &source_p->servptr->serv->client_list);

  assert(!IsUserHostIp(source_p));
  add_user_host(source_p->username, source_p->realhost, 1);
  SetUserHost(source_p);

  if(ConfigFileEntry.enable_cloak_system && !IsIPSpoof(source_p))
  {
    make_virthost(source_p->realhost, source_p->cloaked_host, sizeof(source_p->cloaked_host));
    make_virthost(source_p->sockhost, source_p->cloaked_ip, sizeof(source_p->cloaked_ip));
  }

  if (HasFlag(source_p->servptr, FLAGS_EOB))
    sendto_snomask(SNO_CCONN, L_ALL | SNO_REMOTE,
                   "Client connecting from %s: %s (%s@%s) [%s] [%s]",
                   source_p->servptr->name,
                   source_p->name, source_p->username, source_p->realhost,
                   (EmptyString(source_p->sockhost) || !strcmp("0", source_p->sockhost)) ? "255.255.255.255" : source_p->sockhost,
                   source_p->info);

  introduce_client(source_p);
}

/* introduce_client()
 *
 * inputs	- source_p
 * output	- NONE
 * side effects - This common function introduces a client to the rest
 *		  of the net, either from a local client connect or
 *		  from a remote connect.
 */
static void
introduce_client(struct Client *source_p)
{
  dlink_node *server_node = NULL;
  char ubuf[IRCD_BUFSIZE];

  if (MyClient(source_p))
    send_umode(source_p, source_p, 0, NULL, ubuf);
  else
    send_umode(NULL, source_p, 0, NULL, ubuf);

  watch_check_hash(source_p, RPL_LOGON);

  if (ubuf[0] == '\0')
  {
    ubuf[0] = '+';
    ubuf[1] = '\0';
  }

  DLINK_FOREACH(server_node, serv_list.head)
  {
    struct Client *server = server_node->data;

    if (server == source_p->from)
        continue;

    if (IsCapable(server, CAP_TS6) && HasID(source_p))
      sendto_one(server, ":%s UID %s %d %lu %s %s %s %s %s %s %s :%s",
                   source_p->servptr->id,
                   source_p->name, source_p->hopcount+1,
                   (unsigned long)source_p->tsinfo,
                   ubuf, source_p->username, source_p->host,
                   (MyClient(source_p) && IsIPSpoof(source_p)) ?
                   "0" : source_p->sockhost, source_p->id,
                   source_p->svid,
                   source_p->realhost, source_p->info);
    else
      sendto_one(server, "NICK %s %d %lu %s %s %s %s %s %s :%s",
                   source_p->name, source_p->hopcount+1,
                   (unsigned long)source_p->tsinfo,
                   ubuf, source_p->username, source_p->host,
                   source_p->servptr->name, source_p->svid,
                   source_p->realhost, source_p->info);

    if(!EmptyString(source_p->suser))
      sendto_one(server, ":%s ENCAP * SU %s %s",
        ID_or_name(&me, server), ID_or_name(source_p, server), source_p->suser);

    if (MyClient(source_p))
    {
      char authflags[16], *p = authflags;
      if(IsExemptResv(source_p))
        *p++ = '$';
      if(IsIPSpoof(source_p))
        *p++ = '=';
      if(IsExemptKline(source_p))
        *p++ = '^';
      if(IsExemptGline(source_p))
        *p++ = '_';
      if(IsExemptLimits(source_p))
        *p++ = '>';
      if(IsCanFlood(source_p))
        *p++ = '|';
      *p++ = 0;

      if (*authflags)
        sendto_one(server, ":%s ENCAP * AUTHFLAGS %s %s",
            ID_or_name(&me, server), ID_or_name(source_p, server), authflags);

      if (!EmptyString(source_p->certfp))
        sendto_one(server, ":%s ENCAP * CERTFP %s :%s", ID_or_name(&me, server), ID_or_name(source_p, server), source_p->certfp);
    }
  }
}

/* valid_hostname()
 *
 * Inputs       - pointer to hostname
 * Output       - 1 if valid, 0 if not
 * Side effects - check hostname for validity
 *
 * NOTE: this doesn't allow a hostname to begin with a dot and
 * will not allow more dots than chars.
 */
int
valid_hostname(const char *hostname)
{
  const char *p = hostname;

  assert(p != NULL);

  if (EmptyString(p) || *p == '.' || *p == ':')
    return 0;

  for (; *p; ++p)
    if (!IsHostChar(*p))
      return 0;

  return p - hostname <= HOSTLEN;
}

/* valid_username()
 *
 * Inputs       - pointer to user
 * Output       - 1 if valid, 0 if not
 * Side effects - check username for validity
 *
 * Absolutely always reject any '*' '!' '?' '@' in an user name
 * reject any odd control characters names.
 * Allow '.' in username to allow for "first.last"
 * style of username
 */
int
valid_username(const char *username, const int local)
{
  int dots      = 0;
  const char *p = username;

  assert(p != NULL);

  if (*p == '~')
    ++p;

  /*
   * Reject usernames that don't start with an alphanum
   * i.e. reject jokers who have '-@somehost' or '.@somehost'
   * or "-hi-@somehost", "h-----@somehost" would still be accepted.
   */
  if (!IsAlNum(*p))
    return 0;

  if (local)
  {
    while (*++p)
    {
      if ((*p == '.') && ConfigFileEntry.dots_in_ident)
      {
        if (++dots > ConfigFileEntry.dots_in_ident)
          return 0;
        if (!IsUserChar(*(p + 1)))
          return 0;
      }
      else if (!IsUserChar(*p))
        return 0;
    }
  }
  else
  {
    while (*++p)
      if (!IsUserChar(*p))
        return 0;
  }

  return  p - username <= USERLEN;
}

/* clean_nick_name()
 *
 * input        - nickname
 *              - whether it's a local nick (1) or remote (0)
 * output       - none
 * side effects - walks through the nickname, returning 0 if erroneous
 */
int
valid_nickname(const char *nickname, const int local, const int netadmin)
{
  const char *p = nickname;
  assert(nickname && *nickname);

  /* nicks can't start with a digit or - or be 0 length */
  /* This closer duplicates behaviour of hybrid-6 */
  if (*p == '-' || IsDigit(*p) || *p == '\0')
    return 0;

  for (; *p; ++p)
  {
    if ((!local || netadmin) && *p == (char) 0xA0)
      continue;
    if (!IsNickChar(*p))
      return 0;
  }

  return p - nickname <= NICKLEN;
}

/* report_and_set_user_flags()
 *
 * inputs       - pointer to source_p
 *              - pointer to conf for this user
 * output       - NONE
 * side effects - Report to user any special flags
 *                they are getting, and set them.
 */
static void
report_and_set_user_flags(struct Client *source_p, const struct MaskItem *conf)
{
  /* If this user is being spoofed, tell them so */
  if (IsConfDoSpoofIp(conf))
    sendto_one(source_p,
               ":%s NOTICE %s :*** Spoofing your IP. Congrats.",
               me.name, source_p->name);

  /* If this user is in the exception class, Set it "E lined" */
  if (IsConfExemptKline(conf))
  {
    SetExemptKline(source_p);
    sendto_one(source_p,
               ":%s NOTICE %s :*** You are exempt from K/D/G lines. Congrats.",
               me.name, source_p->name);
  }

  /*
   * The else here is to make sure that G line exempt users
   * do not get noticed twice.
   */
  else if (IsConfExemptGline(conf))
  {
    SetExemptGline(source_p);
    sendto_one(source_p, ":%s NOTICE %s :*** You are exempt from G lines. Congrats.",
               me.name, source_p->name);
  }

  if (IsConfExemptResv(conf))
  {
    SetExemptResv(source_p);
    sendto_one(source_p, ":%s NOTICE %s :*** You are exempt from resvs. Congrats.",
               me.name, source_p->name);
  }

  if (IsConfExemptDnsbl(conf))
  {
    SetExemptDnsbl(source_p);
  }

  /* If this user is exempt from user limits set it "F lined" */
  if (IsConfExemptLimits(conf))
  {
    SetExemptLimits(source_p);
    sendto_one(source_p,
               ":%s NOTICE %s :*** You are exempt from user limits. Congrats.",
               me.name, source_p->name);
  }

  if (IsConfCanFlood(conf))
  {
    SetCanFlood(source_p);
    sendto_one(source_p, ":%s NOTICE %s :*** You are exempt from flood "
               "protection, aren't you fearsome.",
               me.name, source_p->name);
  }
}

/* set_user_mode()
 *
 * added 15/10/91 By Darren Reed.
 * parv[0] - sender
 * parv[1] - username to change mode for
 * parv[2] - modes to change
 */
void
set_user_mode(struct Client *client_p, struct Client *source_p,
              const int parc, char *parv[])
{
  unsigned int flag, setflags;
  struct Snomasks setsnomasks = { 0, 0 };
  char *p, buf[IRCD_BUFSIZE];
  struct Client *target_p;
  int what = MODE_ADD, badflag = 0, i;

  assert(!(parc < 2));

  if ((target_p = find_person(client_p, parv[1])) == NULL)
  {
    if (MyConnect(source_p))
      sendto_one(source_p, form_str(ERR_NOSUCHCHANNEL),
                 me.name, source_p->name, parv[1]);
    return;
  }

  if (source_p != target_p)
  {
     sendto_one(source_p, form_str(ERR_USERSDONTMATCH),
                me.name, source_p->name);
     return;
  }

  if (parc < 3)
  {
    p = buf;
    *p++ = '+';

    for (i = 0; i < 128; ++i)
      if (HasUMode(source_p, user_modes[i]))
        *p++ = (char)i;
    *p = '\0';

    sendto_one(source_p, form_str(RPL_UMODEIS),
               me.name, source_p->name, buf);

    if (MyClient(source_p))
      if (source_p->localClient->snomask.local || source_p->localClient->snomask.remote)
        sendto_one(source_p, form_str(RPL_SNOMASK), me.name, source_p->name, snomask_to_str(&source_p->localClient->snomask));

    return;
  }

  /* find flags already set for user */
  setflags = source_p->umodes;
  if (MyClient(source_p))
    setsnomasks = source_p->localClient->snomask;

  /* parse mode change string(s) */
  for (p = parv[2]; *p; ++p)
  {
    switch (*p)
    {
      case '+':
        what = MODE_ADD;
        break;
      case '-':
        what = MODE_DEL;
        break;
      case 'o':
        if (what == MODE_ADD)
        {
          if (IsServer(client_p) && !HasUMode(source_p, UMODE_OPER))
          {
            ++Count.oper;
            SetOper(source_p);
          }
        }
        else
        {
          deoper(source_p);
        }

        break;

      case 'x':
        if (what == MODE_ADD)
        {
          if (MyClient(source_p) && (!ConfigFileEntry.enable_cloak_system || IsIPSpoof(source_p)))
            break;

          if (HasUMode(source_p, UMODE_CLOAK))
            break;

          AddUMode(source_p, UMODE_CLOAK);

          if (!MyClient(source_p))
            break;

          user_set_hostmask(source_p, NULL, source_p->cloaked_host);

          sendto_server(NULL, CAP_ENCAP | CAP_TS6, NOCAPS,
            ":%s ENCAP * CHGHOST %s %s",
            ID(&me), ID(source_p), source_p->host);
          sendto_server(NULL, CAP_ENCAP, CAP_TS6,
            ":%s ENCAP * CHGHOST %s %s",
            me.name, source_p->name, source_p->host);
        }
        else
        {
          if (!HasUMode(source_p, UMODE_CLOAK))
            break;

          DelUMode(source_p, UMODE_CLOAK);

          if (!MyClient(source_p))
            break;

          user_set_hostmask(source_p, NULL, source_p->realhost);

          sendto_server(NULL, CAP_ENCAP | CAP_TS6, NOCAPS,
            ":%s ENCAP * CHGHOST %s %s",
            ID(&me), ID(source_p), source_p->host);
          sendto_server(NULL, CAP_ENCAP, CAP_TS6,
            ":%s ENCAP * CHGHOST %s %s",
            me.name, source_p->name, source_p->host);
        }
        break;

      case 'S':  /* Only servers may set +S in a burst */
      case 'W':  /* Only servers may set +W in a burst */
      case 'r':  /* Only services may set +r */
      case 'N':
      case 'U':
      case 'q':
        break;

      case 's':
        if (MyClient(source_p))
        {
          if (!HasUMode(source_p, UMODE_OPER) && ConfigFileEntry.oper_only_umodes & UMODE_SERVNOTICE)
          {
            badflag = 1;
            break;
          }

          if (what == MODE_ADD && HasUMode(source_p, UMODE_OPER))
          {
            if (parc > 3)
            {
              struct Snomasks add, remove;
              snomask_parse(parv[3], &add, &remove);

              snomask_set(&source_p->localClient->snomask, &add, &remove);
            }
            else
            {
              struct Snomasks masks;
              masks.local = ConfigFileEntry.oper_snomasks.local;
              masks.remote = ConfigFileEntry.oper_snomasks.remote;

              snomask_set(&source_p->localClient->snomask, &masks, NULL);
            }
          }
          else
          {
            struct Snomasks masks;
            masks.local = SNOMASK_MASK;
            masks.remote = SNOMASK_MASK;

            snomask_set(&source_p->localClient->snomask, NULL, &masks);
          }
        }
        /* FALLTHROUGH */
      default:
        if ((flag = user_modes[(unsigned char)*p]))
        {
          if (MyConnect(source_p) && !HasUMode(source_p, UMODE_OPER) &&
              (ConfigFileEntry.oper_only_umodes & flag))
            badflag = 1;
          else
          {
            if (what == MODE_ADD)
              AddUMode(source_p, flag);
            else
              DelUMode(source_p, flag);
          }
        }
        else if (MyConnect(source_p))
          badflag = 1;

        break;
    }
  }

  if (badflag)
    sendto_one(source_p, form_str(ERR_UMODEUNKNOWNFLAG),
               me.name, source_p->name);

  if (MyConnect(source_p))
  {
    if (HasUMode(source_p, UMODE_ADMIN) && !HasOFlag(source_p, OPER_FLAG_ADMIN))
    {
      sendto_one(source_p, ":%s NOTICE %s :*** You have no admin flag",
                 me.name, source_p->name);
      DelUMode(source_p, UMODE_ADMIN);
    }

    if (snomask_has(&source_p->localClient->snomask, SNO_NCHANGE, 0) && !HasOFlag(source_p, OPER_FLAG_N))
    {
      struct Snomasks masks;

      sendto_one(source_p, ":%s NOTICE %s :*** You have no nchange flag",
                 me.name, source_p->name);

      masks.local = sno_nchange.flag;
      masks.remote = 0;
      snomask_set(&source_p->localClient->snomask, NULL, &masks);
    }

    if (snomask_has(&source_p->localClient->snomask, SNO_CCONN, 1) && !HasOFlag(source_p, OPER_FLAG_FARCONNECT))
    {
      struct Snomasks masks;

      sendto_one(source_p, ":%s NOTICE %s :*** You have insufficient privileges for snomask +C",
                 me.name, source_p->name);

      masks.local = 0;
      masks.remote = sno_cconn.flag;
      snomask_set(&source_p->localClient->snomask, NULL, &masks);
    }

    if (!snomask_has(&setsnomasks, SNO_CCONN, 1) && snomask_has(&source_p->localClient->snomask, SNO_CCONN, 1))
    {
      announce("%s set snomask +s +C", source_p->name);
    }
  }

  if (!(setflags & UMODE_INVISIBLE) && HasUMode(source_p, UMODE_INVISIBLE))
    ++Count.invisi;
  if ((setflags & UMODE_INVISIBLE) && !HasUMode(source_p, UMODE_INVISIBLE))
    --Count.invisi;

  /*
   * compare new flags with old flags and send string which
   * will cause servers to update correctly.
   */
  send_umode_out(client_p, source_p, setflags, &setsnomasks);
}

/* send_umode()
 * send the MODE string for user (user) to connection client_p
 * -avalon
 *
 * inputs	- client_p
 *		- source_p
 *		- int old
 *		- sendmask mask of modes to send
 * 		- suplied umode_buf
 * output	- NONE
 */
void
send_umode(struct Client *client_p, struct Client *source_p,
           unsigned int old, struct Snomasks *oldsnomasks, char *umode_buf)
{
  char *m = umode_buf;
  int what = 0;
  unsigned int i;
  unsigned int flag;

  /*
   * build a string in umode_buf to represent the change in the user's
   * mode between the new (source_p->umodes) and 'old'.
   */
  for (i = 0; i < 128; ++i)
  {
    flag = user_modes[i];
    if (!flag)
      continue;

    if ((flag & old) && !HasUMode(source_p, flag))
    {
      if (what == MODE_DEL)
        *m++ = (char)i;
      else
      {
        what = MODE_DEL;
        *m++ = '-';
        *m++ = (char)i;
      }
    }
    else if (!(flag & old) && HasUMode(source_p, flag))
    {
      if (what == MODE_ADD)
        *m++ = (char)i;
      else
      {
        what = MODE_ADD;
        *m++ = '+';
        *m++ = (char)i;
      }
    }
  }

  *m = '\0';

  if (!client_p && !old) // getting all umodes
    if (oldsnomasks && !oldsnomasks->local && !oldsnomasks->remote) // snomask request
      if (MyClient(source_p) && HasUMode(source_p, UMODE_SERVNOTICE) && (source_p->localClient->snomask.local || source_p->localClient->snomask.remote))
      {
        const char *snomask = snomask_to_str(&source_p->localClient->snomask);
        *m++ = ' ';
        strcpy(m, snomask);
      }

  if (client_p)
  {
    if (*umode_buf)
      sendto_one(client_p, ":%s!%s@%s MODE %s :%s",
                 source_p->name, source_p->username,
                 source_p->host, source_p->name, umode_buf);

    if (oldsnomasks)
      if (oldsnomasks->local != client_p->localClient->snomask.local || oldsnomasks->remote != client_p->localClient->snomask.remote)
        sendto_one(client_p, form_str(RPL_SNOMASK), me.name, client_p->name, snomask_to_str(&client_p->localClient->snomask));
  }
}

/* send_umode_out()
 *
 * inputs	-
 * output	- NONE
 * side effects - Only send ubuf out to servers that know about this client
 */
void
send_umode_out(struct Client *client_p, struct Client *source_p,
               unsigned int old, struct Snomasks *oldsnomask)
{
  char buf[IRCD_BUFSIZE] = { '\0' };
  dlink_node *ptr = NULL;

  send_umode(NULL, source_p, old, NULL, buf);

  if (buf[0])
  {
    DLINK_FOREACH(ptr, serv_list.head)
    {
      struct Client *target_p = ptr->data;

      if ((target_p != client_p) && (target_p != source_p))
        sendto_one(target_p, ":%s MODE %s :%s",
                   ID_or_name(source_p, target_p),
                   ID_or_name(source_p, target_p), buf);
    }
  }

  if (client_p && MyClient(client_p))
    send_umode(client_p, source_p, old, oldsnomask, buf);
}

void
user_set_hostmask(struct Client *target_p, const char *username, const char *hostname)
{
  dlink_node *ptr = NULL;

  if ((!username || !strcmp(target_p->username, username)) && (!hostname || !strcmp(target_p->host, hostname)))
    return;

  if (ConfigFileEntry.cycle_on_host_change)
    sendto_common_channels_local(target_p, 0, 0, ":%s!%s@%s QUIT :Changing hostname",
                                 target_p->name, target_p->username, target_p->host);

  if (username)
    strlcpy(target_p->username, username, sizeof(target_p->username));
  if (hostname)
    strlcpy(target_p->host, hostname, sizeof(target_p->host));

  if (MyClient(target_p))
  {
    if (hostname)
      sendto_one(target_p, form_str(RPL_VISIBLEHOST), me.name,
                 target_p->name, target_p->host);
    clear_ban_cache_client(target_p);
  }

  if (!ConfigFileEntry.cycle_on_host_change)
    return;

  DLINK_FOREACH(ptr, target_p->channel.head)
  {
    char modebuf[PREFIX_NUM + 1], nickbuf[NICKLEN * PREFIX_NUM + PREFIX_NUM] = { '\0' };
    char *p = modebuf;
    int len = 0;
    const struct Membership *ms = ptr->data;

    if (has_member_flags(ms, CHFL_OWNER))
    {
      *p++ = 'q';
      len += snprintf(nickbuf + len, sizeof(nickbuf) - len, len ? " %s" : "%s", target_p->name);
    }

    if (has_member_flags(ms, CHFL_PROTECTED))
    {
      *p++ = 'a';
      len += snprintf(nickbuf + len, sizeof(nickbuf) - len, len ? " %s" : "%s", target_p->name);
    }

    if (has_member_flags(ms, CHFL_CHANOP))
    {
      *p++ = 'o';
      len += snprintf(nickbuf + len, sizeof(nickbuf) - len, len ? " %s" : "%s", target_p->name);
    }

    if (has_member_flags(ms, CHFL_HALFOP))
    {
      *p++ = 'h';
      len += snprintf(nickbuf + len, sizeof(nickbuf) - len, len ? " %s" : "%s", target_p->name);
    }

    if (has_member_flags(ms, CHFL_VOICE))
    {
      *p++ = 'v';
      len += snprintf(nickbuf + len, sizeof(nickbuf) - len, len ? " %s" : "%s", target_p->name);
    }

    *p = '\0';

    sendto_channel_local_butone(target_p, 0, 0, ms->chptr, ":%s!%s@%s JOIN :%s",
                                target_p->name, target_p->username, target_p->host,
                                ms->chptr->chname);
    if (nickbuf[0])
      sendto_channel_local_butone(target_p, 0, 0, ms->chptr, ":%s MODE %s +%s %s",
                                  target_p->servptr->name, ms->chptr->chname,
                                  modebuf, nickbuf);

  }

  if (target_p->away[0])
    sendto_common_channels_local(target_p, 0, CAP_AWAY_NOTIFY,
                                 ":%s!%s@%s AWAY :%s",
                                 target_p->name, target_p->username,
                                 target_p->host, target_p->away);
}

/* user_welcome()
 *
 * inputs	- client pointer to client to welcome
 * output	- NONE
 * side effects	-
 */
static void
user_welcome(struct Client *source_p)
{
#if defined(__TIME__) && defined(__DATE__)
  static const char built_date[] = __DATE__ " at " __TIME__;
#else
  static const char built_date[] = "unknown";
#endif

  if (HasFlag(source_p, FLAGS_SSL))
  {
    AddUMode(source_p, UMODE_SSL);
    sendto_one(source_p, ":%s NOTICE %s :*** Connected securely via %s",
               me.name, source_p->name,
               ssl_get_cipher(source_p->localClient->fd.ssl));

    if (source_p->certfp != NULL)
       sendto_one(source_p,
        ":%s NOTICE %s :*** Your client certificate fingerprint is %s",
        me.name, source_p->name, source_p->certfp);
  }

  if (HasUMode(source_p, UMODE_CLOAK))
    sendto_one(source_p, ":%s NOTICE %s :*** Your host is masked (%s)", me.name,
        source_p->name, source_p->host);

  sendto_one(source_p, form_str(RPL_WELCOME), me.name, source_p->name,
             ServerInfo.network_name, source_p->name);
  sendto_one(source_p, form_str(RPL_YOURHOST), me.name, source_p->name,
             me.name, ircd_version);
  sendto_one(source_p, form_str(RPL_CREATED),
             me.name, source_p->name, built_date);
  sendto_one(source_p, form_str(RPL_MYINFO),
             me.name, source_p->name, me.name, ircd_version, umode_buffer, cmode_buffer);
  show_isupport(source_p);

  if (source_p->id[0] != '\0')
    sendto_one(source_p, form_str(RPL_YOURID), me.name,
               source_p->name, source_p->id);

  show_lusers(source_p);
  motd_signon(source_p);
}

/* check_xline()
 *
 * inputs       - pointer to client to test
 * outupt       - 1 if exiting 0 if ok
 * side effects -
 */
static int
check_xline(struct Client *source_p)
{
  struct MaskItem *conf = NULL;
  const char *reason = NULL;

  if ((conf = find_matching_name_conf(CONF_XLINE, source_p->info, NULL, NULL, 0)))
  {
    ++conf->count;

    if (conf->reason != NULL)
      reason = conf->reason;
    else
      reason = CONF_NOREASON;

    sendto_snomask(SNO_REJ, L_ALL,
                   "X-line Rejecting [%s] [%s], user %s [%s]",
                   source_p->info, reason,
                   get_client_name(source_p, HIDE_IP),
                   source_p->sockhost);

    ++ServerStats.is_ref;
    exit_client(source_p, &me, "Bad user info");
    return 1;
  }

  return 0;
}

/* oper_up()
 *
 * inputs	- pointer to given client to oper
 * output	- NONE
 * side effects	- Blindly opers up given source_p, using conf info
 *                all checks on passwords have already been done.
 *                This could also be used by rsa oper routines.
 */
void
oper_up(struct Client *source_p)
{
  const unsigned int old = source_p->umodes;
  struct Snomasks snomasks, old_snomasks = source_p->localClient->snomask;
  const struct MaskItem *conf = source_p->localClient->confs.head->data;

  assert(source_p->localClient->confs.head);

  ++Count.oper;
  SetOper(source_p);

  if (conf->modes)
    AddUMode(source_p, conf->modes);
  else if (ConfigFileEntry.oper_umodes)
    AddUMode(source_p, ConfigFileEntry.oper_umodes);

  snomasks.local = snomasks.remote = 0;
  if (conf->snomask.local || conf->snomask.remote)
  {
    snomasks.local = conf->snomask.local;
    snomasks.remote = conf->snomask.remote;
  }
  else if (ConfigFileEntry.oper_snomasks.local || ConfigFileEntry.oper_snomasks.remote)
  {
    snomasks.local = ConfigFileEntry.oper_snomasks.local;
    snomasks.remote = ConfigFileEntry.oper_snomasks.remote;
  }
  snomask_set(&source_p->localClient->snomask, &snomasks, NULL);
  if (snomasks.local || snomasks.remote)
    AddUMode(source_p, UMODE_SERVNOTICE);

  if (!(old & UMODE_INVISIBLE) && HasUMode(source_p, UMODE_INVISIBLE))
    ++Count.invisi;
  if ((old & UMODE_INVISIBLE) && !HasUMode(source_p, UMODE_INVISIBLE))
    --Count.invisi;

  assert(dlinkFind(&oper_list, source_p) == NULL);
  dlinkAdd(source_p, make_dlink_node(), &oper_list);

  AddOFlag(source_p, conf->port);

  if (HasOFlag(source_p, OPER_FLAG_ADMIN))
    AddUMode(source_p, UMODE_ADMIN);

  if (snomask_has(&source_p->localClient->snomask, SNO_NCHANGE, 0) && !HasOFlag(source_p, OPER_FLAG_N))
  {
      struct Snomasks masks;
      masks.local = sno_nchange.flag;
      masks.remote = 0;
      snomask_set(&source_p->localClient->snomask, NULL, &masks);
  }

  if (snomask_has(&source_p->localClient->snomask, SNO_CCONN, 1) && !HasOFlag(source_p, OPER_FLAG_FARCONNECT))
  {
      struct Snomasks masks;
      masks.local = 0;
      masks.remote = sno_cconn.flag;
      snomask_set(&source_p->localClient->snomask, NULL, &masks);
  }

  sendto_snomask(SNO_ALL, L_ALL,
                 "%s is now an operator",
                 get_oper_name(source_p));
  send_umode_out(source_p, source_p, old, &old_snomasks);
  sendto_one(source_p, form_str(RPL_YOUREOPER), me.name, source_p->name);

  if (snomask_has(&source_p->localClient->snomask, SNO_CCONN, 1))
    announce("%s is now +s +C", source_p->name);
}

void
deoper(struct Client *source_p)
{
  dlink_node *dm = NULL;
  struct Snomasks mask;

  if (!HasUMode(source_p, UMODE_OPER))
    return;

  ClearOper(source_p);
  Count.oper--;

  if (!MyConnect(source_p))
    return;

  detach_conf(source_p, CONF_OPER);
  ClrOFlag(source_p);
  DelUMode(source_p, ConfigFileEntry.oper_only_umodes);

  mask.local = SNOMASK_MASK;
  mask.remote = SNOMASK_MASK;

  snomask_set(&source_p->localClient->snomask, NULL, &mask);

  if ((dm = dlinkFindDelete(&oper_list, source_p)) != NULL)
    free_dlink_node(dm);
}

int
valid_sid(const char *sid)
{
  if (strlen(sid) == IRC_MAXSID)
    if (IsDigit(*sid))
      if (IsAlNum(*(sid + 1)) && IsAlNum(*(sid + 2)))
        return 1;

  return 0;
}

int
valid_uid(const char *uid)
{
  if (!uid || strlen(uid) != TOTALSIDUID)
    return 0;

  if (!IsDigit(uid[0]))
    return 0;

  for (int i = 1; i < TOTALSIDUID; ++i)
    if (!IsAlNum(uid[i]))
      return 0;

  return 1;
}

static char new_uid[TOTALSIDUID + 1];     /* allow for \0 */

/*
 * init_uid()
 *
 * inputs	- NONE
 * output	- NONE
 * side effects	- new_uid is filled in with server id portion (sid)
 *		  (first 3 bytes) or defaulted to 'A'.
 *	          Rest is filled in with 'A'
 */
void
init_uid(void)
{
  snprintf(new_uid, sizeof(new_uid), "%s999999", ServerInfo.sid);
}

/*
 * add_one_to_uid
 *
 * inputs	- index number into new_uid
 * output	- NONE
 * side effects	- new_uid is incremented by one
 *		  note this is a recursive function
 */
static void
add_one_to_uid(int i)
{
  if (i < IRC_MAXSID)
    return;

  if (new_uid[i] == 'Z')
    new_uid[i] = '0';
  else if (new_uid[i] == '9')
  {
    new_uid[i] = 'A';
    add_one_to_uid(i - 1);
  }
  else
    ++new_uid[i];
}

/*
 * uid_get
 *
 * inputs       - struct Client *
 * output       - new UID is returned to caller
 * side effects - new_uid is incremented by one.
 */
const char *
uid_get(void)
{
  add_one_to_uid(TOTALSIDUID - 1);    /* index from 0 */
  return new_uid;
}

/*
 * init_isupport()
 *
 * input	- NONE
 * output	- NONE
 * side effects	- Must be called before isupport is enabled
 */
void
init_isupport(void)
{
  add_isupport("CALLERID", NULL, -1);
  add_isupport("CASEMAPPING", "rfc1459", -1);
  add_isupport("DEAF", "D", -1);
  add_isupport("KICKLEN", NULL, KICKLEN);
  add_isupport("MODES", NULL, MAXMODEPARAMS);
#ifdef CHANAQ
# ifdef HALFOPS
  add_isupport("PREFIX", "(qaohv)~&@%+", -1);
  add_isupport("STATUSMSG", "~&@%+", -1);
# else
  add_isupport("PREFIX", "(qaohv)~&@+", -1);
  add_isupport("STATUSMSG", "~&@+", -1);
# endif
#else
# ifdef HALFOPS
  add_isupport("PREFIX", "(ohv)@%+", -1);
  add_isupport("STATUSMSG", "@%+", -1);
# else
  add_isupport("PREFIX", "(ov)@+", -1);
  add_isupport("STATUSMSG", "@+", -1);
# endif
#endif
  add_isupport("EXCEPTS", "e", -1);
  add_isupport("INVEX", "I", -1);
}

/*
 * add_isupport()
 *
 * input	- name of supported function
 *		- options if any
 *		- number if any
 * output	- NONE
 * side effects	- Each supported item must call this when activated
 */
void
add_isupport(const char *name, const char *options, int n)
{
  dlink_node *ptr;
  struct Isupport *support;

  DLINK_FOREACH(ptr, support_list.head)
  {
    support = ptr->data;
    if (irccmp(support->name, name) == 0)
    {
      MyFree(support->name);
      MyFree(support->options);
      break;
    }
  }

  if (ptr == NULL)
  {
    support = MyMalloc(sizeof(*support));
    dlinkAddTail(support, &support->node, &support_list);
  }

  support->name = xstrdup(name);
  if (options != NULL)
    support->options = xstrdup(options);
  support->number = n;

  rebuild_isupport_message_line();
}

/*
 * delete_isupport()
 *
 * input	- name of supported function
 * output	- NONE
 * side effects	- Each supported item must call this when deactivated
 */
void
delete_isupport(const char *name)
{
  dlink_node *ptr;
  struct Isupport *support;

  DLINK_FOREACH(ptr, support_list.head)
  {
    support = ptr->data;
    if (irccmp(support->name, name) == 0)
    {
      dlinkDelete(ptr, &support_list);
      MyFree(support->name);
      MyFree(support->options);
      MyFree(support);
      break;
    }
  }

  rebuild_isupport_message_line();
}

/*
 * rebuild_isupport_message_line
 *
 * input	- NONE
 * output	- NONE
 * side effects	- Destroy the isupport MessageFile lines, and rebuild.
 */
void
rebuild_isupport_message_line(void)
{
  char isupportbuffer[IRCD_BUFSIZE];
  char *p = isupportbuffer;
  dlink_node *ptr = NULL, *ptr_next = NULL;
  int n = 0;
  int tokens = 0;
  size_t len = 0;
  size_t reserve = strlen(me.name) + HOSTLEN + strlen(form_str(RPL_ISUPPORT));

  DLINK_FOREACH_SAFE(ptr, ptr_next, support_list_lines.head)
  {
    dlinkDelete(ptr, &support_list_lines);
    MyFree(ptr->data);
    free_dlink_node(ptr);
  }

  DLINK_FOREACH(ptr, support_list.head)
  {
    struct Isupport *support = ptr->data;

    p += (n = sprintf(p, "%s", support->name));
    len += n;

    if (support->options != NULL)
    {
      p += (n = sprintf(p, "=%s", support->options));
      len += n;
    }

    if (support->number > 0)
    {
      p += (n = sprintf(p, "=%d", support->number));
      len += n;
    }

    *p++ = ' ';
    len++;
    *p = '\0';

    if (++tokens == (MAXPARA-2) || len >= (sizeof(isupportbuffer)-reserve))
    { /* arbritrary for now */
      if (*--p == ' ')
        *p = '\0';

      dlinkAddTail(xstrdup(isupportbuffer), make_dlink_node(), &support_list_lines);
      p = isupportbuffer;
      len = 0;
      n = tokens = 0;
    }
  }

  if (len != 0)
  {
    if (*--p == ' ')
      *p = '\0';
    dlinkAddTail(xstrdup(isupportbuffer), make_dlink_node(), &support_list_lines);
  }
}

/* is 'target' on the target list of 'source'? if so, moves to the beginning and returns 1 */
static int
target_check(struct Client *source_p, unsigned int target)
{
  unsigned int max = IRCD_MIN(GlobalSetOptions.max_targets, MAXTARGETS);

  if (!max || source_p->localClient->targets[0] == target)
    return 1;

  for (unsigned int i = 1; i < max; ++i)
    if (source_p->localClient->targets[i] == target)
    {
      memmove(&source_p->localClient->targets[1], &source_p->localClient->targets[0], sizeof(unsigned int) * i);
      source_p->localClient->targets[0] = target;
      return 1;
    }

  return 0;
}

/* adds a target. return 1 if no space, otherwise target is added and returns 0 */
static int
target_add(struct Client *source_p, unsigned int target, int bump_time)
{
  unsigned int max = IRCD_MIN(GlobalSetOptions.max_targets, MAXTARGETS);

  if (!max)
    return 0;

  if (bump_time && CurrentTime < source_p->localClient->next_target)
    return 1;

  if (CurrentTime > source_p->localClient->next_target + (GlobalSetOptions.target_delay * max))
    source_p->localClient->next_target = CurrentTime - (GlobalSetOptions.target_delay * max);

  if (bump_time)
    source_p->localClient->next_target += GlobalSetOptions.target_delay;

  memmove(&source_p->localClient->targets[1], &source_p->localClient->targets[0], sizeof(unsigned int) * (max - 1));
  source_p->localClient->targets[0] = target;

  return 0;
}

int
target_check_client(struct Client *source, struct Client *target)
{
  if (!ConfigFileEntry.use_target_change || HasFlag(target, FLAGS_SERVICE))
    return 0;

  unsigned int targetv = strhash(ID(target));

  /* existing target? */
  if (target_check(source, targetv))
    return 0;

  int bump = 1;

  /* if the target has messaged the source recently, allow it and don't use a target slot */
  unsigned int max = IRCD_MIN(GlobalSetOptions.max_reply_targets, MAX_REPLY_TARGETS);
  for (unsigned int i = 0; i < max; ++i)
    if (source->localClient->reply_targets[i] == targetv)
    {
      bump = 0;
      break;
    }

  /* opers and users with exempt limits bypass target checking */
  if (bump && (HasUMode(source, UMODE_OPER) || IsExemptLimits(source)))
    bump = 0;

  /* allowing_channel bypasses using a target slot */
  if (bump && target_find_allowing_channel(source, target))
    bump = 0;

  /* no choice, use target slot */
  return target_add(source, targetv, bump);
}

int
target_check_channel(struct Client *source, struct Channel *target)
{
  if (!ConfigFileEntry.use_target_change)
    return 0;

  unsigned int targetv = strhash(target->chname);

  if (target_check(source, targetv))
    return 0;

  return target_add(source, targetv, 1);
}

/* is source +vhoaq in a channel that target is in? */
int
target_find_allowing_channel(struct Client *source, struct Client *target)
{
  dlink_node *node;
  DLINK_FOREACH(node, source->channel.head)
  {
    struct Membership *mem = node->data;

    if (mem->flags & (CHFL_OWNER | CHFL_PROTECTED | CHFL_CHANOP | CHFL_HALFOP | CHFL_VOICE))
      if (IsMember(target, mem->chptr))
        return 1;
  }

  return 0;
}

/* has target recently messaged source? */
int
target_recently_messaged(struct Client *source, struct Client *target)
{
  unsigned int max = IRCD_MIN(GlobalSetOptions.max_targets, MAXTARGETS),
               sourcev = strhash(ID(source));

  for (unsigned int i = 0; i < max; ++i)
    if (target->localClient->targets[i] == sourcev)
      return 1;

  return 0;
}

/* add a reply target 'target' to source */
void
target_add_reply(struct Client *source_p, struct Client *target)
{
  unsigned int max = IRCD_MIN(GlobalSetOptions.max_reply_targets, MAX_REPLY_TARGETS),
               targetv = strhash(ID(target));

  if (!max)
    return;

  for (unsigned int i = 0; i < max; ++i)
    if (source_p->localClient->reply_targets[i] == targetv)
    {
      if (i)
      {
        memmove(&source_p->localClient->reply_targets[1], &source_p->localClient->reply_targets[0], sizeof(unsigned int) * i);
        source_p->localClient->reply_targets[0] = targetv;
      }
      return;
    }

  memmove(&source_p->localClient->reply_targets[1], &source_p->localClient->reply_targets[0], sizeof(unsigned int) * (max - 1));
  source_p->localClient->reply_targets[0] = targetv;
}
